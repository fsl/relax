/*  despot.cc

    Saad Jbabdi, FMRIB Analysis Group
    Sean Deoni, FMRIB Physics Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#include <cmath>
#include <stdlib.h>

#include "utils/options.h"

#include "despotOptions.h"
#include "despotModel.h"

using namespace DESPOT;


int main(int argc, char** argv)
{
  //parse command line 
  despotOptions& opts = despotOptions::getInstance();
  if(!opts.parse_command_line(argc,argv)) return 1;

  if(opts.mode.value()=="despot1"){
    Despot1 despot1;
    despot1.fit();
    despot1.save();
  }
  else if(opts.mode.value()=="despot2"){
    Despot2 despot2;
    despot2.fit();
    despot2.save();
  }
  else if(opts.mode.value()=="despot1_hifi"){
    Despot1_HIFI despot1_hifi;
    despot1_hifi.fit();
    despot1_hifi.save();
  }
  else if(opts.mode.value()=="mcdespot"){
    cerr << "sorry, mcdespot is not implemented yet." << endl;
    exit(1);
  }
  else{
    if(opts.mode.value()=="")
      cout << "exit without doing anything. mode not set!" << endl;
    else{
      cout << "unkown mode {" << opts.mode.value() << "}." << endl;
      cout << "please type despot --help for a list of available modes." << endl;
    }
  }
  
  return 0;

}













