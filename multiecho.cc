/*  multiecho.cc

    Saad Jbabdi, FMRIB Analysis Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#include <cmath>
#include <stdlib.h>

#include "utils/options.h"

#include "multiechoOptions.h"
#include "multiechoModel.h"

using namespace MULTIECHO;


int main(int argc, char** argv)
{
  //parse command line 
  multiechoOptions& opts = multiechoOptions::getInstance();
  if(!opts.parse_command_line(argc,argv)) return 1;

  if(opts.mode.value()=="spinecho"){
    if(opts.tefile.value()==""){
      cerr<<"--te option required for spinecho mode"<<endl;
      return -1;
    }
    SpinEcho se;
    se.fit();
    se.save();
  }
  else if(opts.mode.value()=="ir"){
    if(opts.tifile.value()==""){
      cerr<<"--ti option required for ir mode"<<endl;
      return -1;
    }
    InversionRecovery ir;
    ir.fit();
    ir.save();
  }
  else{
    if(opts.mode.value()=="")
      cout << "exit without doing anything. mode not set!" << endl;
    else{
      cout << "unkown mode {" << opts.mode.value() << "}." << endl;
      cout << "please type multiecho --help for a list of available modes." << endl;
    }
  }
  
  return 0;

}













