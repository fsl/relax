/*  despotOptions.cc

    Saad Jbabdi, FMRIB Analysis Group
    Sean Deoni, FMRIB Physics Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#define WANT_STREAM
#define WANT_MATH

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "despotOptions.h"
#include "utils/options.h"

using namespace Utilities;

namespace DESPOT {

  despotOptions* despotOptions::gopt = NULL;
  
  bool despotOptions::parse_command_line(int argc, char** argv){

    for(int a = options.parse_command_line(argc, argv); a < argc; a++);
    
    // help or no compulsory options
    if(help.value() || !options.check_compulsory_arguments() || argc<=1){
      options.usage();
      return false;
    }
    // check compatibility between inputs
    if(spgrdatafile.value()!="" && spgrfafile.value()==""){
      cerr << "Please provide a flip angle file for SPGR data" << endl;
      return false;
    }
    if(ssfpdatafile.value()!="" && ssfpfafile.value()==""){
      cerr << "Please provide a flip angle file for SSFP data" << endl;
      return false;
    }
    if(irspgrdatafile.value()!="" && irspgrfafile.value()==""){
      cerr << "Please provide a flip angle file for IR-SPGR data" << endl;
      return false;
    }
    if(mode.value()=="despot1" && spgrdatafile.value()==""){
      cerr << "You have requested despot1. Please provide SPGR data" << endl;
      return false;
    }
    if(mode.value()=="despot2" && ssfpdatafile.value()==""){
      cerr << "You have requested despot2. Please provide SSFP data" << endl;
      return false;
    }
    if(mode.value()=="despot2" && spgrdatafile.value()=="" && t1file.value()==""){
      cerr << "You have requested despot2. Please provide either SPGR data or a T1 map" << endl;
      return false;
    }
    if(mode.value()=="despot1_hifi" && (irspgrdatafile.value()=="" || spgrdatafile.value()=="")){
      cerr << "You have requested despot1_hifi. Please provide SPGR and IR-SPGR data" << endl;
      return false;
    }
    if(mode.value()=="despot1" && tr1.value()<0){
      cerr << "please set the repetition time for SPGR data" << endl;
      return false;
    }
    if(mode.value()=="despot2" && (tr2.value()<0 || tr1.value()<0)){
      cerr << "please set the repetition time for both SPGR and SSFP data" << endl;
      return false;
    }
    if(mode.value()=="despot1_hifi" && (irtr.value()<0 || irspgrtifile.value()=="")){
      cerr << "please set the repetition time and inversion times for IR-SPGR data" << endl;
      return false;
    }
    if(mode.value()=="mcdespot" && (spgrdatafile.value()=="" || ssfpdatafile.value()=="")){
      cerr << "You have requested mcdespot. Please provide spgr and ssfp data" << endl;
      return false;
    }
    

    return true;
  }

}







