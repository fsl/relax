include $(FSLCONFDIR)/default.mk

PROJNAME = relax
XFILES   = despot multiecho
LIBS     = -lfsl-newimage -lfsl-miscmaths -lfsl-utils -lfsl-newran \
           -lfsl-NewNifti -lfsl-znz -lfsl-cprob

despot: despot.o despotOptions.o despotModel.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

multiecho: multiecho.o multiechoOptions.o multiechoModel.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
