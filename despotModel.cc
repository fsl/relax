
/*  despotModel.cc

    Saad Jbabdi, FMRIB Analysis Group
    Sean Deoni, FMRIB Physics Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */


#include "despotModel.h"

using namespace NEWMAT;
using namespace NEWIMAGE;
using namespace NEWRAN;
using namespace MISCMATHS;

// transform flip angles from degrees to radians
void Despot::deg2rad(){
  for(unsigned int i=0;i<m_fa.size();i++)
    for(int j=1;j<=m_fa[i].Ncols();j++)
      m_fa[i](1,j) *= M_PI/180.0;

  return;
}

///////////////////////////////////////////////////////
//                                                   //
//                 THIS IS DESPOT 1                  //
//                                                   //
///////////////////////////////////////////////////////

// fitting procedure for despot1
// the signal equation is:
// S = M0*(1-E1)*sin(fa)/(1-E1*cos(fa))
// where E1=exp(-tr/t1)
void Despot1::fit(){
  if(opts.verbose.value()) cout << "fitting despot1" << endl;

  Matrix M(m_fa[0].Ncols(),2);
  ColumnVector b(m_fa[0].Ncols());

  ColumnVector sinfa(M.Nrows());
  ColumnVector tanfa(M.Nrows());
  DiagonalMatrix w(M.Nrows()); // weights for weighted least squares
  double ERNST;


  deg2rad();
  for(int i=1;i<=M.Nrows();i++){
    sinfa(i) = sin(m_fa[0](1,i));
    tanfa(i) = tan(m_fa[0](1,i));
    M(i,2)   = 1.0;
  }

  ColumnVector sol(2);
  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++)
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=m_fa[0].Ncols();i++){
	  M(i,1) = (*m_data[0])(x,y,z,i-1)/tanfa(i);
	  b(i) = (*m_data[0])(x,y,z,i-1)/sinfa(i);
	}
	sol = pinv(M)*b;


	if(opts.do_wls.value()){
	  // iterate weighted least squares
	  for(int iter=1;iter<=5;iter++){
	    ERNST = acos(sol(1));
	    if(ERNST>1e-16){
	      for(int i=1;i<=M.Nrows();i++)
		w(i,i) = (*m_data[0])(x,y,z,i-1)/ERNST;
	      sol = pinv(w*M)*w*b;
	    }
	  }
	}
	if(sol(1)>0 && sol(1)<1)
	  m_t1map(x,y,z) = -m_tr/log(sol(1));
	if(sol(1)<1)
	  m_m0map(x,y,z) = sol(2)/(1-sol(1));
      }
  }

}

// This function iterates a Gibb's sampler and takes the mean
void Despot1::fit_gibbs(){
  if(opts.verbose.value()) cout << "fitting despot1 using gibbs" << endl;
  
  Random::Set(0.5);

  ColumnVector Y(m_fa[0].Ncols());
  Matrix       M(m_fa[0].Ncols(),2);
  M = 1.0;

  ColumnVector sinfa(M.Nrows());
  ColumnVector tanfa(M.Nrows());

  double sigma2 = 0;
  SymmetricMatrix sigma_hat(2);
  ColumnVector c_hat(2),sol(2);

  double t1,m0;

  deg2rad();
  for(int i=1;i<=M.Nrows();i++){
    sinfa(i) = sin(m_fa[0](1,i));
    tanfa(i) = tan(m_fa[0](1,i));
  }

  int gibbsiter = 10;
  ColumnVector t1_samples(gibbsiter);
  ColumnVector m0_samples(gibbsiter);

  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++)
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=M.Nrows();i++){
	  Y(i)    = (*m_data[0])(x,y,z,i-1) / sinfa(i); 
	  M(i,1)  = (*m_data[0])(x,y,z,i-1) / tanfa(i);
	}
	sol = pinv(M)*Y;
	// initialise gibbs using least squares	
	if(sol(1)<=0) sol(1) = 1e-6;
	if(sol(1)>=1) sol(1) = 1-1e-6;
	if(sol(2)<=0) sol(2) = 1e-6;
	t1 = -m_tr/log(sol(1));
	m0 = sol(2)/(1-sol(1));

	t1_samples(1) = t1;
	m0_samples(1) = m0;
	// start gibbs
	for(int iter=2;iter<=gibbsiter;iter++){
	  sigma_hat << (M.t()*M).i();
	  c_hat     = sigma_hat*(M.t()*Y);
	  // sample sigma2
	  Gamma g((float(m_fa[0].Ncols())-2.0)/2.0+1.0);
	  sigma2 = g.Next();
	  sigma2 = (.5*((sol-c_hat).t()*M.t()*M*(sol-c_hat)).AsScalar()+1.0)/sigma2;
	  // sample t1 and m0
	  sol = mvnrnd(c_hat.t(),sigma2*sigma_hat).t();
	  // priors
	  if(sol(1)>0 && sol(1)<1)
	    t1 = -m_tr/log(sol(1));
	  if(sol(2)>0 && sol(1)<1)
	    m0 = sol(2)/(1-sol(1));

	  t1_samples(iter) = t1;
	  m0_samples(iter) = m0;
	  
	}
	m_t1map(x,y,z) = t1_samples.Sum()/float(gibbsiter);
	m_m0map(x,y,z) = m0_samples.Sum()/float(gibbsiter);

      }
  }
  

}

void Despot1::save(){
  m_t1map.setDisplayMaximumMinimum(3000,0);
  save_volume(m_t1map,opts.obase.value()+"_despot1_t1map");

  m_m0map.setDisplayMaximumMinimum(m_m0map.max(),0);
  save_volume(m_m0map,opts.obase.value()+"_despot1_m0map");
}

///////////////////////////////////////////////////////
//                                                   //
//                 THIS IS DESPOT 2                  //
//                                                   //
///////////////////////////////////////////////////////

// fitting procedure for despot2
// the signal equation is:
// S = M0*(1-E1)*sin(fa)/(1-E1E2-(E1-E2)cos(fa))
// where
//    E1=exp(-tr1/t1)
//    E2=exp(-tr2/t2)
void Despot2::fit(){
  if(m_calcboth)
    fit_both();
  else
    fit_t2only();

  return;
} 
void Despot2::fit_t2only(){
  if(opts.verbose.value()) cout << "fitting despot2 (t2 only)" << endl;

  Matrix M(m_fa[0].Ncols(),2);
  ColumnVector b(m_fa[0].Ncols());

  ColumnVector sinfa(M.Nrows());
  ColumnVector tanfa(M.Nrows());
  double e1,e2;
  double testval;

  DiagonalMatrix w(M.Nrows()); // weights for weighted least squares
  double ERNST;

  deg2rad();
  for(int i=1;i<=M.Nrows();i++){
    sinfa(i) = sin(m_fa[0](1,i));
    tanfa(i) = tan(m_fa[0](1,i));
    M(i,2)   = 1.0;
  }

  ColumnVector sol(2);
  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++)
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=M.Nrows();i++){
	  M(i,1) = (*m_data[0])(x,y,z,i-1) / tanfa(i);  
	  b(i)   = (*m_data[0])(x,y,z,i-1) / sinfa(i);
	}
	sol = pinv(M)*b;

	e1 = exp(-m_tr1/m_t1map(x,y,z));
	testval = (sol(1)-e1)/(sol(1)*e1-1);

	if(opts.do_wls.value()){
	  // iterate weighted least squares
	  for(int iter=1;iter<=10;iter++){
	    ERNST = acos((e1-testval)/(1-e1*testval));
	    if(ERNST>1e-16){
	      for(int i=1;i<=M.Nrows();i++)
		w(i,i) = (*m_data[0])(x,y,z,i-1)/ERNST;
	      sol = pinv(w*M)*w*b;
	      testval = (sol(1)-e1)/(sol(1)*e1-1);
	    }
	  }
	}

	if(testval>0 && testval<1)
	  m_t2map(x,y,z) = -m_tr2 / log(testval);

	e2 = exp(-m_tr2 / m_t2map(x,y,z));
	
	m_m0map2(x,y,z) = sol(2)*(1-e1*e2)/(1-e1);

      }// x loop
  }// z loop

  return;
}

// solves for both t1 and t2
void Despot2::fit_both(){
  if(opts.verbose.value()) cout << "fitting despot2 (both t1 and t2)" << endl;

  int npts = m_fa[0].Ncols() + m_fa[1].Ncols();

  Matrix M(npts,4);
  Matrix b(npts,2);

  ColumnVector sinfa(M.Nrows());
  ColumnVector tanfa(M.Nrows());
  double e1,e2;
  double testval;

  M=0.0;
  b=0.0;

  deg2rad();
  for(int i=1;i<=m_fa[1].Ncols();i++){
    sinfa(i) = sin(m_fa[1](1,i));
    tanfa(i) = tan(m_fa[1](1,i));
    M(i,2)   = 1.0;
  }
  for(int i=1,j=m_fa[1].Ncols()+1;i<=m_fa[0].Ncols();i++,j++){
    sinfa(j) = sin(m_fa[0](1,i));
    tanfa(j) = tan(m_fa[0](1,i));
    M(j,4)   = 1.0;
  }
  
  
  Matrix sol(4,2);
  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++)
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=m_fa[1].Ncols();i++){
	  M(i,1) = (*m_data[1])(x,y,z,i-1) / tanfa(i);  
	  b(i,1)   = (*m_data[1])(x,y,z,i-1) / sinfa(i);
	}
	for(int i=1,j=m_fa[1].Ncols()+1;i<=m_fa[0].Ncols();i++,j++){
	  M(j,3) = (*m_data[0])(x,y,z,i-1) / tanfa(j);  
	  b(j,2)   = (*m_data[0])(x,y,z,i-1) / sinfa(j);
	}
	sol = pinv(M)*b;

	if(sol(1,1)>0 && sol(1,1)<1)
	  m_t1map(x,y,z) = -m_tr1 / log(sol(1,1));
	if(sol(1,1)<1)
	  m_m0map1(x,y,z) = sol(2,1)/(1-sol(1,1));

	e1 = sol(1,1);

	testval = (sol(3,2)-e1)/(sol(3,2)*e1-1);
	if(testval>0)
	  m_t2map(x,y,z) = -m_tr2 / log(testval);

	e2 = exp(-m_tr2 / m_t2map(x,y,z));
	
	m_m0map2(x,y,z) = sol(4,2)*(1-e1*e2)/(1-e1);


      }// x loop
  }// z loop

  return;

}
void Despot2::save(){
  m_t2map.setDisplayMaximumMinimum(3000,0);
  save_volume(m_t2map,opts.obase.value()+"_despot2_t2map");

  if(m_calcboth){
    m_t1map.setDisplayMaximumMinimum(3000,0);
    save_volume(m_t1map,opts.obase.value()+"_despot2_t1map");
    m_m0map1.setDisplayMaximumMinimum(m_m0map1.max(),0);
    save_volume(m_m0map1,opts.obase.value()+"_despot2_m0map1");
  }
  m_m0map2.setDisplayMaximumMinimum(m_m0map2.max(),0);
  save_volume(m_m0map2,opts.obase.value()+"_despot2_m0map2");
}



//////////////////////////////////////////////////////////////
//                                                          //
//                 THIS IS DESPOT 1 - HIFI                  //
//                                                          //
//////////////////////////////////////////////////////////////

void Despot1_HIFI::fit(){
  if(opts.verbose.value()) cout << "fitting despot1_hifi" << endl;

  ColumnVector Y(m_fa[0].Ncols());
  ColumnVector Y_ir(m_fa[1].Ncols());

  // convert flip angles to radians
  deg2rad();

  ColumnVector fa=m_fa[0].Row(1).t();
  ColumnVector fa_ir=m_fa[1].Row(1).t();
  ColumnVector ti=m_ti.Row(1).t();
  ColumnVector irtr=m_irtr;

  

  ////////////////////////////////
  // Parameters for initialisation
  Matrix M(m_fa[0].Ncols(),2);
  ColumnVector b(m_fa[0].Ncols());
  ColumnVector sinfa(M.Nrows());
  ColumnVector tanfa(M.Nrows());
  for(int i=1;i<=M.Nrows();i++){
    sinfa(i) = sin(m_fa[0](1,i));
    tanfa(i) = tan(m_fa[0](1,i));
    M(i,2)   = 1.0;
  }
  ColumnVector sol(2);
  ////////////////////////////////
  float init_kappa=1.0;
  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++)
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=m_fa[0].Ncols();i++){
	  Y(i) = (*m_data[0])(x,y,z,i-1); 
	  M(i,1) = Y(i)/tanfa(i);
	  b(i) = Y(i)/sinfa(i);
	}
	sol = pinv(M)*b;
	 
	for(int i=1;i<=m_fa[1].Ncols();i++){
	  Y_ir(i) = (*m_data[1])(x,y,z,i-1);
	}
	

	// start using least squares on spgr and kappa=mean(solution ir-equation)
	ColumnVector start(3);
	if(sol(1)>0 && sol(1)<1)
	  start << sol(2)/(1.0-sol(1)) << sol(1) << init_kappa;
	else
	  start << 500.0 << .99 << 1.0;

	//	start << 2000.0 << .9802 << .85;

	//cout << "----------------------" << endl;
	//OUT(sol.t());
 	//OUT(start.t());


	HifiNonlinCF hifi_cf(Y,Y_ir,fa,fa_ir,ti,m_tr,m_irtr,opts.m0fudge.value(),m_cosfactor);
	

 	NonlinParam  lmpar(3,NL_LM,start); // 3 parameters - Levenberg-Marquardt
 	lmpar.SetStartingEstimate(start);
	
 	__attribute__((unused)) NonlinOut status = nonlin(lmpar,hifi_cf);
	// As the next section has been commented out, tell the compiler it is not used.
// 	if(status==NL_MAXITER)
// 	  cerr << "too many iterations...." << endl;

 	ColumnVector final_par = lmpar.Par();

 	//OUT(final_par.t());
//  	cout << "m0=" << final_par(1) << endl;
//  	cout << "t1=" << -m_tr/log(final_par(2)) << endl;
//  	cout << "b1=" << final_par(3) << endl;

	//cout << "----------------------" << endl;

// 	if(final_par(1)>m_m0max)
// 	  final_par << m_m0max << 0 << 0;

// 	if(final_par(1)>10000)
// 	  final_par << 10000 << 0 << 0;

	m_m0map(x,y,z) = final_par(1);
	m_t1map(x,y,z) = -m_tr/log(final_par(2));
	m_kmap(x,y,z)  = final_par(3);


      }
  }

}

void Despot1_HIFI::save(){
  m_t1map.setDisplayMaximumMinimum(3000,0);
  save_volume(m_t1map,opts.obase.value()+"_despot1hifi_t1map");

  m_m0map.setDisplayMaximumMinimum(m_m0map.max(),0);
  save_volume(m_m0map,opts.obase.value()+"_despot1hifi_m0map");

  m_kmap.setDisplayMaximumMinimum(1,0);
  save_volume(m_kmap,opts.obase.value()+"_despot1hifi_kmap");

}


//////////////////////////
// Non linear optimisation 
//////////////////////////
double HifiNonlinCF::cf(const NEWMAT::ColumnVector& p)const{
  // p(1) = rho
  // p(2) = e1
  // p(3) = k

  double cfv = 0.0;
  double err;


  // SPGR cost
  for(int i=1;i<=Y.Nrows();i++){
    err = abs((p(1)*(1.0-p(2))*sin(p(3)*fa(i)))/(1.0-p(2)*cos(p(3)*fa(i)))) - Y(i);

    //err = (rho*(1-e1)*sin(k*fa(i))/(1-e1*cos(k*fa(i)))) - Y(i);

    cfv += err*err; 
  }
  // IR-SPGR cost
  for(int i=1;i<=Y_ir.Nrows();i++){
    err = abs(m0fudge*p(1)*sin(p(3)*fa_ir(i))*(1.0-cosfactor*exp(ti_ratio(i)*log(p(2)))+exp(tr_ratio(i)*log(p(2))))) - Y_ir(i);

    //err = (m0fudge*rho*sin(k*hfa)*(1.0-(1.0-cos(170.0*M_PI/180.0))*exp(-hti/t1)+exp(-hirtr/t1))) - Y_ir(i);

    cfv += err*err; 
  }
  // calculate penalty term
//   double penalty=en_cst;
//   for(int i=1;i<=p.Nrows();i++){
//     penalty += (.5*p(i)*(p(i)-2.0*prior_means(i))/prior_sigma2s(i));
//   }

  //OUT(cfv);

  //cfv = npts*log(cfv/2) + penalty;

  //OUT(cfv);
  //cout<<"=============="<<endl;
  //  exit(1);
  return(cfv);
}
NEWMAT::ReturnMatrix HifiNonlinCF::grad(const NEWMAT::ColumnVector& p)const{
  NEWMAT::ColumnVector gradv(p.Nrows());
  gradv = 0.0;
  double dval = 0.0;
  
  
  double sig;

  // SPGR grad
  for(int i=1;i<=Y.Nrows();i++){
    dval = ((p(1)*(1.0-p(2))*sin(p(3)*fa(i)))/(1.0-p(2)*cos(p(3)*fa(i))));
    sig  = sign(dval);
    dval = abs(dval) - Y(i);

    //ss += dval*dval;
    
    gradv(1) += sig * dval * (dval+Y(i))/p(1);
    gradv(2) += sig * dval * (dval+Y(i))*(cos(p(3)*fa(i))-1.0)/((1.0-p(2))*(1.0-p(2)*cos(p(3)*fa(i))));
    gradv(3) += sig * dval * (dval+Y(i))*fa(i)*(cos(p(3)*fa(i))-p(2))/(sin(p(3)*fa(i))*(1.0-p(2)*cos(p(3)*fa(i))));
  }
  // IR-SPGR grad
  for(int i=1;i<=Y_ir.Nrows();i++){
     dval = m0fudge*p(1)*sin(p(3)*fa_ir(i))*(1.0-cosfactor*exp(ti_ratio(i)*log(p(2)))+exp(tr_ratio(i)*log(p(2))));
     sig  = sign(dval);
     dval = abs(dval) - Y_ir(i);


     gradv(1) += sig * dval * (dval+Y_ir(i))/p(1);
     gradv(2) += sig * dval * m0fudge* p(1) * sin(p(3)*fa_ir(i)) * (tr_ratio(i)*exp((tr_ratio(i)-1.0)*log(p(2))) - cosfactor*ti_ratio(i)*exp((ti_ratio(i)-1.0)*log(p(2))));
     gradv(3) += sig * dval * (dval+Y_ir(i)) * fa_ir(i) / tan(p(3)*fa_ir(i));

  }

  // for(int i=1;i<=p.Nrows();i++)
//     gradv(i) = npts*gradv(i)/ss + (p(i)-prior_means(i))/prior_sigma2s(i);
  

  gradv.Release();
  return(gradv);
}
// this uses Gauss-Newton approximation
boost::shared_ptr<BFMatrix> HifiNonlinCF::hess(const NEWMAT::ColumnVector&   p,
                                            boost::shared_ptr<BFMatrix>    iptr)const{
  boost::shared_ptr<BFMatrix>   hessm;

  if (iptr && iptr->Nrows()==(unsigned int)p.Nrows() && iptr->Ncols()==(unsigned int)p.Nrows()) hessm = iptr;
  else hessm = boost::shared_ptr<BFMatrix>(new FullBFMatrix(p.Nrows(),p.Nrows()));
  
  double dval;
  double sig;

  Matrix J(Y.Nrows()+Y_ir.Nrows(),3);
  for(int i=1;i<=Y.Nrows();i++){
    dval = ((p(1)*(1.0-p(2))*sin(p(3)*fa(i)))/(1.0-p(2)*cos(p(3)*fa(i))));
    sig  = sign(dval);
    dval = abs(dval) - Y(i);

    J(i,1) = sig * (dval+Y(i))/p(1);
    J(i,2) = sig * (dval+Y(i))*(cos(p(3)*fa(i))-1.0)/((1.0-p(2))*(1.0-p(2)*cos(p(3)*fa(i))));
    J(i,3) = sig * (dval+Y(i))*fa(i)*(cos(p(3)*fa(i))-p(2))/(sin(p(3)*fa(i))*(1.0-p(2)*cos(p(3)*fa(i))));
  }

  for(int i=1,j=Y.Nrows()+1;i<=Y_ir.Nrows();i++,j++){
    dval = m0fudge*p(1)*sin(p(3)*fa_ir(i))*(1.0-cosfactor*exp(ti_ratio(i)*log(p(2)))+exp(tr_ratio(i)*log(p(2))));
    sig  = sign(dval);
    dval = abs(dval) - Y_ir(i);

    J(j,1) = sig * (dval+Y_ir(i))/p(1);;
    J(j,2) = sig * m0fudge * p(1) * sin(p(3)*fa_ir(i)) * (tr_ratio(i)*exp((tr_ratio(i)-1)*log(p(2))) - cosfactor*ti_ratio(i)*exp((ti_ratio(i)-1.0)*log(p(2))));
    J(j,3) = sig * (dval+Y_ir(i)) * fa_ir(i) / tan(p(3)*fa_ir(i));

  }

 //  for(int i=1;i<=J.Nrows();i++)
//     for(int j=1;j<=J.Ncols();j++)
//       J(i,j) = npts*J(i,j)/ss + (p(j)-prior_means(j))/prior_sigma2s(j);  
  
  
  for (int i=1; i<=p.Nrows(); i++){
    for (int j=i; j<=p.Nrows(); j++){
      dval = 0.0;
      for(int k=1;k<=J.Nrows();k++)
	dval += J(k,i)*J(k,j);
      hessm->Set(i,j,dval);
    }
  }

  for (int j=1; j<=p.Nrows(); j++) {
    for (int i=j+1; i<=p.Nrows(); i++) {
      hessm->Set(i,j,hessm->Peek(j,i));
    }
  }


  return(hessm);
}
