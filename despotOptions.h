/*  despotOptions.h

    Saad Jbabdi, FMRIB Analysis Group
    Sean Deoni, FMRIB Physics Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(despotOptions_h)
#define despotOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"

using namespace Utilities;

namespace DESPOT {

class despotOptions {
 public:
  static despotOptions& getInstance();
  ~despotOptions() { delete gopt; }

  // some options
  Option<bool> verbose;
  Option<bool> help;
  // inputs
  Option<string> spgrdatafile;
  Option<string> ssfpdatafile;
  Option<string> irspgrdatafile;
  Option<string> spgrfafile;
  Option<string> ssfpfafile;
  Option<string> irspgrfafile;
  Option<string> maskfile;
  Option<string> irspgrtifile; 
  Option<string> t1file;
  Option<string> b1file;
  Option<float>  tr1;
  Option<float>  tr2;
  Option<float>  irtr;
  Option<int>    nrf;
  Option<float>  tifudge;
  Option<float>  m0fudge;
  Option<float>  effinv;
  // outputs
  Option<string> obase;
  // some other options
  Option<string> mode;
  Option<bool> do_wls;
  

  bool parse_command_line(int argc, char** argv);
  
 private:
  despotOptions();  
  const despotOptions& operator=(despotOptions&);
  despotOptions(despotOptions&);

  OptionParser options; 
      
  static despotOptions* gopt;
  
};

 inline despotOptions& despotOptions::getInstance(){
   if(gopt == NULL)
     gopt = new despotOptions();
   
   return *gopt;
 }

 inline despotOptions::despotOptions() :
   verbose(string("-v,--verbose"), false, 
	  string("switch on diagnostic messages"), 
	  false, no_argument),
   help(string("-h,--help"), false,
	string("display this message"),
	false, no_argument),
   spgrdatafile(string("--d1,--spgr"), string(""),
	       string("spgr data file"),
	       false, requires_argument),  
   ssfpdatafile(string("--d2,--ssfp"), string(""),
	       string("ssfp data file"),
	       false, requires_argument),  
   irspgrdatafile(string("--ird1,--irspgr"), string(""),
	       string("ir-spgr data file"),
	       false, requires_argument),  
   spgrfafile(string("--fa1,--spgrfa"), string(""),
	       string("spgr flip angles file (degrees)"),
	       false, requires_argument),  
   ssfpfafile(string("--fa2,--ssfpfa"), string(""),
	       string("ssfp flip angles file (degrees)"),
	       false, requires_argument),  
   irspgrfafile(string("--irfa,--irspgrfa"), string(""),
	       string("ir-spgr flip angles file (degrees)"),
	       false, requires_argument),  
   maskfile(string("-m,--mask"), string(""),
	    string("bet binary mask file"),
	    false, requires_argument),
   irspgrtifile(string("--ti,--irspgrti"), string(""),
	    string("inversion times file as an input to despot1_hifi"),
	    false, requires_argument),
   t1file(string("--t1,--t1map"), string(""),
	    string("t1 map file as an input to despot2"),
	    false, requires_argument),
   b1file(string("--b1,--b1map"), string(""),
	    string("b1 map file as a correction for flip angles (multiplicative)"),
	    false, requires_argument),
   tr1(string("--tr1,--spgrtr"), -1,
	    string("repetition time for spgr data (output t1 map will be in the same units)"),
	    false, requires_argument),
   tr2(string("--tr2,--ssfptr"), -1,
	    string("repetition time for ssfp data (in the same units as tr1)"),
	    false, requires_argument),
   irtr(string("--irtr,--irspgrtr"), -1,
	    string("repetition time for ir-spgr data (in the same units as tr1)"),
	    false, requires_argument),
   nrf(string("--nrf,--numRFpulses"), -1,
	    string("number of RF pulses for ir-spgr data"),
	    false, requires_argument),
   tifudge(string("--tifudge"), 1,
	    string("fudge factor for the actual ti (default = 1.0)"),
	    false, requires_argument),
   m0fudge(string("--m0fudge"), 1,
	    string("fudge factor for M0 (default = 1.0)"),
	    false, requires_argument),
   effinv(string("--effectiveinversion"), 180.0,
	    string("effective inversion (default = 180.0 degrees)"),
	    false, requires_argument),
   obase(string("-o,--out"), string("despot"),
	 string("output basename - default='despot'"),
	 false, requires_argument),
   mode(string("--mode"), "",
	 string("which mode? available options are: despot1, despot2, despot1_hifi, mcdespot"),
	 false, requires_argument),
   do_wls(string("--wls"), false,
	 string("do weighted least squares - default=no"),
	 false, no_argument),
   options("despot calculates T1/T2 maps using DESPOT.", "despot [listOfOptions].\n")
   {
         
     try {
       options.add(verbose);
       options.add(help);
       options.add(spgrdatafile);
       options.add(ssfpdatafile);
       options.add(irspgrdatafile);
       options.add(spgrfafile);
       options.add(ssfpfafile);
       options.add(irspgrfafile);
       options.add(maskfile);
       options.add(irspgrtifile);
       options.add(t1file);
       options.add(b1file);
       options.add(tr1);
       options.add(tr2);
       options.add(irtr);
       options.add(nrf);
       options.add(tifudge);
       options.add(m0fudge);
       options.add(effinv);
       options.add(obase);
       options.add(mode);
       options.add(do_wls);
       
     }
     catch(X_OptionError& e) {
       options.usage();
       cerr << endl << e.what() << endl;
     } 
     catch(std::exception &e) {
       cerr << e.what() << endl;
     }    
     
   }
}

#endif





