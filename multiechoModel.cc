
/*  multiechoModel.cc

    Saad Jbabdi, FMRIB Analysis Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */


#include "multiechoModel.h"

using namespace NEWMAT;
using namespace NEWIMAGE;
using namespace MISCMATHS;


// fitting procedure for multiecho
// the signal equation is:
// S = S0*exp(-te/t2)
void SpinEcho::fit(){
  if(opts.verbose.value()) cout << "fitting multiecho" << endl;

  Matrix M(m_te.Ncols(),2);
  for(int i=1;i<=m_te.Ncols();i++){
    M(i,1) = -m_te(1,i);
    M(i,2) = 1.0;
  }
  m_pinvM = pinv(M);

  ColumnVector b(m_te.Ncols());

  ColumnVector sol(2);
  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++)
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=m_te.Ncols();i++){
	  b(i) = log(m_data(x,y,z,i-1));
	}
	sol = m_pinvM*b;
	
	m_t2(x,y,z) = 1/sol(1);
	m_S0(x,y,z) = exp(sol(2));
	
	if(opts.savepred.value()){
	  for(int i=1;i<=m_te.Ncols();i++)
	    m_pred(x,y,z,i-1) = exp(M(i,1)*sol(1) + M(i,2)*sol(2));
	}
      }
  }

}

void SpinEcho::save(){
  if(opts.verbose.value()) cout << "saving results" << endl;

  m_t2.setDisplayMaximumMinimum(100,0);
  save_volume(m_t2,opts.obase.value()+"_t2");

  m_S0.setDisplayMaximumMinimum(m_S0.max(),0);
  save_volume(m_S0,opts.obase.value()+"_s0");
  
  if(opts.savepred.value()){
    m_pred.setDisplayMaximumMinimum(m_pred.max(),0);
    save_volume4D(m_pred,opts.obase.value()+"_pred");
  }
}

// fitting procedure for multiecho
// the signal equation is:
// S = A+B*exp(-C*ti)  where C=exp(Ct)
void InversionRecovery::fit(){
  if(opts.verbose.value()) cout << "fitting IR" << endl;

  // // Matrix M(m_ti.Ncols(),5);
  // // double ti;
  // // for(int i=1;i<=m_ti.Ncols();i++){
  // //   ti=m_ti(1,i);
  // //   M(i,1) = 1.0;
  // //   M(i,2) = ti;
  // //   M(i,3) = ti*ti/2.0;
  // //   M(i,4) = ti*ti*ti/6.0;
  // //   M(i,5) = ti*ti*ti*ti/24.0;
  // // }
  // // m_pinvM = pinv(M);
  //OUT(M);
  //OUT(m_pinvM);
  //exit(1);


  ColumnVector sol(5);ColumnVector vec(3);
  m_Y.ReSize(m_ti.Ncols());
  for(int z=0;z<m_mask.zsize();z++){
    cout << "processing slice number " << z << endl;
    for(int y=0;y<m_mask.ysize();y++){
      for(int x=0;x<m_mask.xsize();x++){
	if(m_mask(x,y,z)==0)continue;
	
	for(int i=1;i<=m_ti.Ncols();i++){
	  m_Y(i) = m_data(x,y,z,i-1);
	}

	// initialise using Taylor trick
	//sol = m_pinvM*m_Y;
	//float tmp=sqrt(2*abs(sol(3)));
	//tmp = tmp>1e-4?tmp:1e-4;
	//tmp = tmp<1e-2?tmp:1e-2;
	//vec << sol(1) << -sol(2)/tmp << log(tmp);
	vec << m_Y.Maximum() << m_Y.Minimum()-m_Y.Maximum() << -std::log(500);
	
	// solve nonlinear
	NonlinParam  lmpar(vec.Nrows(),NL_LM); 
	lmpar.SetGaussNewtonType(LM_L);
	lmpar.SetStartingEstimate(vec);

	NonlinOut status;
	status = nonlin(lmpar,(*this));

	vec = lmpar.Par();


	//OUT(b.t());
	//OUT(sol.t());
	m_A(x,y,z) = vec(1);
	m_B(x,y,z) = vec(2);
	m_t1(x,y,z) = exp(-vec(3));
	if(opts.savepred.value()){
	  for(int i=1;i<=m_ti.Ncols();i++){
	    m_pred(x,y,z,i-1) = vec(1)+vec(2)*exp(-exp(vec(3))*m_ti(1,i));
	  }
	}
      }
    }
  }

}



void InversionRecovery::save(){
  if(opts.verbose.value()) cout << "saving results" << endl;

  m_t1.setDisplayMaximumMinimum(1000,0);
  save_volume(m_t1,opts.obase.value()+"_t1");

  m_A.setDisplayMaximumMinimum(m_A.max(),0);
  save_volume(m_A,opts.obase.value()+"_A");

  m_B.setDisplayMaximumMinimum(m_B.max(),0);
  save_volume(m_B,opts.obase.value()+"_B");
  
  if(opts.savepred.value()){
    m_pred.setDisplayMaximumMinimum(m_pred.max(),0);
    save_volume4D(m_pred,opts.obase.value()+"_pred");
  }
}


// Nonlinear fitting
NEWMAT::ReturnMatrix InversionRecovery::forwardModel(const NEWMAT::ColumnVector& p)const{

  ColumnVector pred(m_ti.Ncols());
  for(int i=1;i<=pred.Nrows();i++){
    pred(i) = p(1)+p(2)*std::exp(-std::exp(p(3))*m_ti(1,i));
  }  
  pred.Release();
  return pred;
}
double InversionRecovery::cf(const NEWMAT::ColumnVector& p)const{
  double cfv = 0.0,err=0.0;

  for(int i=1;i<=m_Y.Nrows();i++){
    err= (p(1)+p(2)*std::exp(-std::exp(p(3))*m_ti(1,i)))-m_Y(i);
    cfv += err*err;
  }
  return(cfv);
}
NEWMAT::ReturnMatrix InversionRecovery::grad(const NEWMAT::ColumnVector& p)const{
  NEWMAT::ColumnVector gradv(p.Nrows());
  gradv = 0.0;
  Matrix J(m_ti.Ncols(),3);
  ColumnVector diff(m_ti.Ncols());
  float sig;
  for(int i=1;i<=m_Y.Nrows();i++){
    sig=p(1)+p(2)*std::exp(-std::exp(p(3))*m_ti(1,i));
    diff(i) = sig-m_Y(i);
    J(i,1) = 1.0;
    J(i,2) = (sig-p(1))/p(2);
    J(i,3) = -std::exp(p(3))*m_ti(1,i)*(sig-p(1));    
  }
  gradv = 2.0*J.t()*diff;

  gradv.Release();
  return gradv;
}
boost::shared_ptr<BFMatrix> InversionRecovery::hess(const NEWMAT::ColumnVector& p,boost::shared_ptr<BFMatrix> iptr)const{

  boost::shared_ptr<BFMatrix>   hessm;
  if (iptr && iptr->Nrows()==(unsigned int)p.Nrows() && iptr->Ncols()==(unsigned int)p.Nrows()) hessm = iptr;
  else hessm = boost::shared_ptr<BFMatrix>(new FullBFMatrix(p.Nrows(),p.Nrows()));

  Matrix J(m_Y.Nrows(),3);
  float sig;

  for(int i=1;i<=m_Y.Nrows();i++){
    sig=p(1)+p(2)*std::exp(-std::exp(p(3))*m_ti(1,i));
    J(i,1) = 1.0;
    J(i,2) = (sig-p(1))/p(2);
    J(i,3) = -std::exp(p(3))*m_ti(1,i)*(sig-p(1));   
  }

  for (int i=1; i<=p.Nrows(); i++){
    for (int j=i; j<=p.Nrows(); j++){
      sig = 0.0;
      for(int k=1;k<=J.Nrows();k++)
	sig += 2.0*(J(k,i)*J(k,j));
      hessm->Set(i,j,sig);
    }
  }
  for (int j=1; j<=p.Nrows(); j++) {
    for (int i=j+1; i<=p.Nrows(); i++) {
      hessm->Set(i,j,hessm->Peek(j,i));
    }
  }
  return(hessm);  
}

