/*  multiechoModel.h

    Saad Jbabdi, FMRIB Analysis Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(multiecho1_h)
#define multiecho1_h

#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/miscprob.h"
#include "miscmaths/nonlin.h"
#include "miscmaths/minimize.h"
#include "utils/options.h"

#include "multiechoOptions.h"

using namespace NEWIMAGE;
using namespace MISCMATHS;
using namespace NEWMAT;


// Model:  S=S0*exp(-TE/T2)
// Params: S0, T2
class SpinEcho {
public:
  SpinEcho():opts(MULTIECHO::multiechoOptions::getInstance()){
    if(opts.verbose.value()) cout << "reading data and brain mask" << endl;
    read_volume(m_mask,opts.maskfile.value());


    read_volume4D(m_data,opts.datafile.value());

    m_te = read_ascii_matrix(opts.tefile.value());

    if(m_te.Nrows()>1) m_te=m_te.t();
    if(m_te.Nrows()>1){
      cerr << "Error: TE file must be either row or column vector" << endl;
      exit(1);
    }

    if(m_te.Ncols() != (int)m_data.tsize()){
      cerr << "Error: data and TE file are not compatible." << endl;
      exit(1);
    }

    m_S0.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    copybasicproperties(m_mask,m_S0);
    m_S0=0;

    m_t2.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    copybasicproperties(m_mask,m_t2);
    m_t2=0;
    if(opts.savepred.value()){
      m_pred.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize(),m_data.tsize());
      copybasicproperties(m_mask,m_pred);
      m_pred=0;
    }

  }
  ~SpinEcho(){}

  void fit();
  void save();

 protected:
  MULTIECHO::multiechoOptions& opts;
  volume4D<float>              m_data;
  volume4D<float>              m_pred;
  Matrix                       m_te;
  volume<float>                m_mask;

  volume<float>                m_t2;
  volume<float>                m_S0;
  Matrix                       m_pinvM;


};



// Model: S = A+B*exp(-C*TI)
// Params: A, B, C=1/T1=exp(Ct)  [positive constraint on C]
class InversionRecovery : public NonlinCF{
public:
  InversionRecovery():opts(MULTIECHO::multiechoOptions::getInstance()){
    if(opts.verbose.value()) cout << "reading data and brain mask" << endl;
    read_volume(m_mask,opts.maskfile.value());


    read_volume4D(m_data,opts.datafile.value());

    m_ti = read_ascii_matrix(opts.tifile.value());

    if(m_ti.Nrows()>1) m_ti=m_ti.t();
    if(m_ti.Nrows()>1){
      cerr << "Error: TI file must be either row or column vector" << endl;
      exit(1);
    }

    if(m_ti.Ncols() != (int)m_data.tsize()){
      cerr << "Error: data and TI file are not compatible." << endl;
      OUT(m_ti.Ncols());
      OUT(m_data.tsize());
      exit(1);
    }

    m_A.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    copybasicproperties(m_mask,m_A);
    m_A=0;

    m_B.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    copybasicproperties(m_mask,m_B);
    m_B=0;

    m_t1.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    copybasicproperties(m_mask,m_t1);
    m_t1=0;

    if(opts.savepred.value()){
      m_pred.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize(),m_data.tsize());
      copybasicproperties(m_mask,m_pred);
      m_pred=0;
    }

  }
  ~InversionRecovery(){}


  // routines from NonlinCF
  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  boost::shared_ptr<BFMatrix> hess(const NEWMAT::ColumnVector&p,boost::shared_ptr<BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;
  NEWMAT::ReturnMatrix forwardModel(const NEWMAT::ColumnVector& p)const;



  void fit();
  void save();

 protected:
  MULTIECHO::multiechoOptions& opts;
  volume4D<float>              m_data;
  volume4D<float>              m_pred;
  Matrix                       m_ti;
  volume<float>                m_mask;
  Matrix                       m_pinvM;
  volume<float>                m_t1;
  volume<float>                m_A;
  volume<float>                m_B;
  ColumnVector                 m_Y;


};




#endif
