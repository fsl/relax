/*  multiechoOptions.h

    Saad Jbabdi, FMRIB Analysis Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(multiechoOptions_h)
#define multiechoOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"

using namespace Utilities;

namespace MULTIECHO {

class multiechoOptions {
 public:
  static multiechoOptions& getInstance();
  ~multiechoOptions() { delete gopt; }

  // some options
  Option<bool> verbose;
  Option<bool> help;
  // inputs
  Option<string> datafile;
  Option<string> tefile;
  Option<string> tifile;
  Option<string> maskfile;
  // outputs
  Option<bool>   savepred;
  Option<string> obase;
  Option<string> mode;
  

  bool parse_command_line(int argc, char** argv);
  
 private:
  multiechoOptions();  
  const multiechoOptions& operator=(multiechoOptions&);
  multiechoOptions(multiechoOptions&);

  OptionParser options; 
      
  static multiechoOptions* gopt;
  
};

 inline multiechoOptions& multiechoOptions::getInstance(){
   if(gopt == NULL)
     gopt = new multiechoOptions();
   
   return *gopt;
 }

 inline multiechoOptions::multiechoOptions() :
   verbose(string("-v,--verbose"), false, 
	  string("switch on diagnostic messages"), 
	  false, no_argument),
   help(string("-h,--help"), false,
	string("display this message"),
	false, no_argument),
   datafile(string("-d,--data"), string(""),
	       string("multiple echo data file"),
	       true, requires_argument),  
   tefile(string("--te,--echotimes"), string(""),
	       string("echo times (output T2 map will be in the same units)"),
	       false, requires_argument),  
   tifile(string("--ti,--inversiontimes"), string(""),
	       string("inversion times (output T1 map will be in the same units)"),
	       false, requires_argument),  
   maskfile(string("-m,--mask"), string(""),
	       string("brain mask"),
	       true, requires_argument),
   savepred(string("--savepred"), false,
	       string("save predicted signal"),
	       false, no_argument),
   obase(string("-o,--basename"), string("multiecho"),
	       string("output basename [default = multiecho]"),
	       true, requires_argument),
   mode(string("--mode"), string("spinecho"),
	       string("desired mode [spinecho (default) o ir]"),
	       false, requires_argument),
   options("multiecho calculates T1/T2 maps using MULTIECHO.", "multiecho [listOfOptions].\n")
   {
         
     try {
       options.add(verbose);
       options.add(help);
       options.add(datafile);
       options.add(tefile);
       options.add(tifile);
       options.add(maskfile);
       options.add(savepred);
       options.add(obase);
       options.add(mode);
     }
     catch(X_OptionError& e) {
       options.usage();
       cerr << endl << e.what() << endl;
     } 
     catch(std::exception &e) {
       cerr << e.what() << endl;
     }    
     
   }
}

#endif





