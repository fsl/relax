/*  despotModel.h

    Saad Jbabdi, FMRIB Analysis Group
    Sean Deoni, FMRIB Physics Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(despot1_h)
#define despot1_h

#include "armawrap/newmat.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/miscprob.h"
#include "miscmaths/nonlin.h"
#include "miscmaths/minimize.h"
#include "utils/options.h"
#include "newran/newran.h"

#include "despotOptions.h"

using namespace NEWIMAGE;
using namespace MISCMATHS;
using namespace NEWMAT;
using namespace NEWRAN;

// generic class for despot1,2,hifi,mc
// contains data, flip angles, brain mask, and volume info
// assumes that everything is in the same space
class Despot {
public:
  Despot():opts(DESPOT::despotOptions::getInstance()){
    if(opts.verbose.value()) cout << "reading brain mask" << endl;

    if(opts.maskfile.value() == ""){
      cerr << "please set a correct brain mask" << endl;
      exit(1);
    }

    read_volume(m_mask,opts.maskfile.value());

  }
  virtual ~Despot(){}

  virtual void fit()=0;
  virtual void save()=0;
  void deg2rad();

 protected:
  DESPOT::despotOptions& opts;
  vector< volume4D<float>* > m_data;
  vector< NEWMAT::Matrix >   m_fa;
  volume<float>              m_mask;


};

// here we deal with size compatibilities
class Despot1 : Despot {
public:
  Despot1():Despot(),m_tr(opts.tr1.value()){
    // read data and flip angles
    if(opts.verbose.value())cout << "reading data and flip angles" << endl;

    volume4D<float> *tmpdata = new volume4D<float>;
    read_volume4D(*tmpdata,opts.spgrdatafile.value());
    m_data.push_back(tmpdata);

    Matrix fa = read_ascii_matrix(opts.spgrfafile.value());
    if(fa.Nrows()>1) fa=fa.t();
    if(fa.Nrows()>1){
      cerr << "flip angles should be a single row/column file" << endl;
      exit(1);
    }
    m_fa.push_back(fa);

    if( m_fa[0].Ncols() != (*m_data[0]).tsize()){
      cerr << "number of flip angles and number of data volumes must be equal!" << endl;
      exit(1);
    }

    m_t1map.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    m_m0map.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());

    copybasicproperties(m_mask,m_t1map);
    copybasicproperties(m_mask,m_m0map);

    m_t1map=0;
    m_m0map=0;


    m_m0max = (*m_data[0]).max()*10;
  }
  void fit();
  void fit_gibbs();
  void save();

  friend class Despot1_HIFI;

private:
  volume<float> m_t1map;
  volume<float> m_m0map;
  float         m_tr;
  float         m_m0max;

};

class Despot2 : Despot {
public:
  Despot2():Despot(),m_tr1(opts.tr1.value()),m_tr2(opts.tr2.value()){
    // read data and flip angles
    if(opts.verbose.value())cout << "reading data and flip angles" << endl;

    // In this class, the ssfp data comes first within the member m_data.
    // Then the spgr data is read if provided by the user, otherwise the t1map is loaded in instead.

    // first load ssfp
    if(opts.ssfpdatafile.value()!=""){
      volume4D<float> *tmpdata = new volume4D<float>;
      read_volume4D(*tmpdata,opts.ssfpdatafile.value());
      m_data.push_back(tmpdata);

      Matrix fa = read_ascii_matrix(opts.ssfpfafile.value());

      if(fa.Nrows()>1) fa=fa.t();
      if(fa.Nrows()>1){
	cerr << "flip angles should be a single row/column file" << endl;
	exit(1);
      }
      m_fa.push_back(fa);

    }
    // now load spgr if available
    if(opts.spgrdatafile.value()!=""){
      m_calcboth = true;

      volume4D<float> *tmpdata = new volume4D<float>;
      read_volume4D(*tmpdata,opts.spgrdatafile.value());
      m_data.push_back(tmpdata);

      Matrix fa = read_ascii_matrix(opts.spgrfafile.value());

      if(fa.Nrows()>1) fa=fa.t();
      if(fa.Nrows()>1){
	cerr << "flip angles should be a single row/column file" << endl;
	exit(1);
      }
      m_fa.push_back(fa);

      m_t1map.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
      copybasicproperties(m_mask,m_t1map);
      m_t1map=0;

      m_m0map1.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
      copybasicproperties(m_mask,m_m0map1);
      m_m0map1=0;
    }
    //otherwise, load t1map
    else{
      m_calcboth = false;
      read_volume(m_t1map,opts.t1file.value());
    }

    for(unsigned int i=0;i<m_fa.size();i++)
      if( m_fa[i].Ncols() != (*m_data[i]).tsize()){
	cerr << "number of flip angles and number of data volumes must be equal!" << endl;
	exit(1);
      }


    m_t2map.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    m_m0map2.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());


    copybasicproperties(m_mask,m_t2map);
    copybasicproperties(m_mask,m_m0map2);


    m_t2map=0;
    m_m0map2=0;
  }
  void fit();
  void fit_both();
  void fit_t2only();
  void save();

  friend class MCdespot;

private:
  volume<float> m_t1map;
  volume<float> m_t2map;
  volume<float> m_m0map1;
  volume<float> m_m0map2;
  float         m_tr1;
  float         m_tr2;
  bool          m_calcboth;

};

// nonlinear class for despot1_hifi
class HifiNonlinCF : public NonlinCF {
public:
  HifiNonlinCF(const ColumnVector& iY,const ColumnVector& iY_ir,
	       const ColumnVector& ifa,const ColumnVector& ifa_ir,
	       const ColumnVector& iti,const float& itr,const ColumnVector& itr_ir,const float& im0fudge,const float& icosfactor):Y(iY),Y_ir(iY_ir),fa(ifa),fa_ir(ifa_ir),ti(iti),tr(itr),tr_ir(itr_ir),m0fudge(im0fudge),cosfactor(icosfactor){


    tr_ratio.ReSize(ti.Nrows());
    ti_ratio.ReSize(ti.Nrows());

    for(int i=1;i<=ti.Nrows();i++){
      tr_ratio(i) = tr_ir(i)/tr;
      ti_ratio(i) = ti(i)/tr;
    }

    npts    = float(Y.Ncols()+Y_ir.Ncols());
    nparams = 3;

    // set priors
    prior_means.ReSize(nparams);
    prior_means << 2000 << exp(-tr/500.0) << 1.0;

    prior_sigma2s.ReSize(nparams);
    prior_sigma2s << 5000 << 1 << 1;

    // set constants
    en_cst = lgam(npts/2.0);
    for(int i=1;i<=nparams;i++)
      en_cst += (-.5*log(2.0*M_PI*prior_sigma2s(i))) -.5*prior_means(i)*prior_means(i)/prior_sigma2s(i);


    //cout<<"I am going to start optimising!"<<endl;
  }
  ~HifiNonlinCF(){}

  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  boost::shared_ptr<BFMatrix> hess(const NEWMAT::ColumnVector&p,boost::shared_ptr<BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;
  void set_priors(const ColumnVector& m,const ColumnVector& s2){prior_means=m;prior_sigma2s=s2;}
private:
  const ColumnVector& Y;
  const ColumnVector& Y_ir;
  const ColumnVector& fa;
  const ColumnVector& fa_ir;
  const ColumnVector& ti;

  float tr;
  ColumnVector tr_ir;
  ColumnVector tr_ratio;
  ColumnVector ti_ratio;

  float npts;
  int   nparams;
  float m0fudge;
  float cosfactor;

  // priors
  ColumnVector prior_means;
  ColumnVector prior_sigma2s;

  // constants for energy calculation (update if priors changed)
  float en_cst;

};


// In this class, the data are spgr and irspgr
class Despot1_HIFI : Despot1 {
public:
  Despot1_HIFI():Despot1(){


    // now we read the IR-SPGR data and their flip angles
    volume4D<float> *tmpdata = new volume4D<float>;
    read_volume4D(*tmpdata,opts.irspgrdatafile.value());
    m_data.push_back(tmpdata);

    Matrix fa = read_ascii_matrix(opts.irspgrfafile.value());
    if(fa.Nrows()>1) fa=fa.t();
    if(fa.Nrows()>1){
      cerr << "flip angles should be a single row/column file" << endl;
      exit(1);
    }
    m_fa.push_back(fa);
    if( m_fa[1].Ncols() != (*m_data[1]).tsize()){
      cerr << "number of flip angles and number of data volumes in IR-SPGR must be equal!" << endl;
      exit(1);
    }

    // we also need the inversion times
    m_ti = read_ascii_matrix(opts.irspgrtifile.value());
    if(m_ti.Nrows()>1) m_ti=m_ti.t();
    if(m_ti.Nrows()>1){
      cerr << "inversion times should be a single row/column file" << endl;
      exit(1);
    }
    if(m_ti.Ncols() != (*m_data[1]).tsize()){
      cerr << "number of inversion times and number of data volumes in IR-SPGR must be equal!" << endl;
      exit(1);
    }

    // initialise bias field
    m_kmap.reinitialize(m_mask.xsize(),m_mask.ysize(),m_mask.zsize());
    copybasicproperties(m_mask,m_kmap);
    m_kmap=0;

    // apply various corrections prescribed by Sean
    // first the ti's
    m_ti *= opts.tifudge.value(); // 95% of the prescribed ti on a Siemens scanner (Sean's simulations)

    // then the tr
    if(opts.nrf.value()<=0){
      cerr << "please set a correct value for the number of RF pulses in the IR-SPGR data"<<endl;
      exit(1);
    }
    m_irtr.ReSize(m_ti.Ncols());
    for(int i=1;i<=m_ti.Ncols();i++){
      m_irtr(i) = m_ti(1,i) + float(opts.nrf.value()*opts.irtr.value());
    }
    m_cosfactor = 1-cos(opts.effinv.value()*M_PI/180.0);

  }
  void fit();
  void save();
private:
  Matrix        m_ti;
  ColumnVector  m_irtr;
  volume<float> m_kmap;
  float         m_cosfactor;

};

class MCdespotNonlinCF : public NonlinCF {
  public:
  MCdespotNonlinCF(){}

  ~MCdespotNonlinCF(){}

  NEWMAT::ReturnMatrix grad(const NEWMAT::ColumnVector& p)const;
  boost::shared_ptr<BFMatrix> hess(const NEWMAT::ColumnVector&p,boost::shared_ptr<BFMatrix> iptr)const;
  double cf(const NEWMAT::ColumnVector& p)const;


private:

};


// In this class, the data are spgr and ssfp
class MCdespot : Despot2 {
public:
  MCdespot():Despot2(){}

  void fit();
  void save();



private:
  volume<float> m_mwf;


};

#endif
