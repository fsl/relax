/*  multiechoOptions.cc

    Saad Jbabdi, FMRIB Analysis Group

    Copyright (C) 2008 University of Oxford  */

/*  CCOPYRIGHT  */

#define WANT_STREAM
#define WANT_MATH

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "multiechoOptions.h"
#include "utils/options.h"

using namespace Utilities;

namespace MULTIECHO {

  multiechoOptions* multiechoOptions::gopt = NULL;
  
  bool multiechoOptions::parse_command_line(int argc, char** argv){

    for(int a = options.parse_command_line(argc, argv); a < argc; a++);
    
    // help or no compulsory options
    if(help.value() || !options.check_compulsory_arguments() || argc<=1){
      options.usage();
      return false;
    }


    return true;
  }

}







